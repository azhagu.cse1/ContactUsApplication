﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactUs.Interface
{
    /// <summary>
    /// This is interface class for file creation.
    /// </summary>
    public interface ICreateFile
    {
        bool CreateFile(string path, object obj);
    }
}
