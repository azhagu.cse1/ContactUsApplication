﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ContactUs.Models;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using ContactUs.Interface;
using ContactUs.Files;

namespace ContactUs.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly IWebHostEnvironment _hostingEnvironment;

        private readonly ICreateFile _createFile;

        public HomeController(ILogger<HomeController> logger, IWebHostEnvironment hostingEnvironment, ICreateFile createFile)
        {
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
            _createFile = createFile;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(ContactViewModel vm)
        {
            _logger.LogInformation("Post is started");

            if (ModelState.IsValid)
            {
                try
                {
                    var webRootPath = _hostingEnvironment.WebRootPath + $"\\ContactUs_{DateTime.Now.ToString("yyyymmddhhmmss")}.txt";
                    if (_createFile.CreateFile(webRootPath, vm))
                    {
                        ViewBag.Message = "Thank you for Contacting us ";
                    }
                    else
                    {
                        ViewBag.Message = $" Sorry Please try again after sometime.";
                    }
                    ModelState.Clear();
                }
                catch (Exception ex)
                {
                    ModelState.Clear();
                    ViewBag.Message = $" Sorry Please try again after sometime.";
                    _logger.LogCritical($" Error is occured during the process { ex.ToString()}");
                }
            }

            _logger.LogInformation("Post is done");

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
