﻿using ContactUs.Interface;
using ContactUs.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ContactUs.Files
{
    /// <summary>
    /// This class is create text file.
    /// </summary>
    public class CreateTextFile : ICreateFile
    {
        /// <summary>
        /// This class return true if text file is created successfully otherwise false.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool CreateFile(string path, object obj)
        {
            bool isReturn = false;

            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                fs.Close();

                if (obj is ContactViewModel)
                {
                    using (StreamWriter sw = new StreamWriter(path))
                    {
                        var vm = (ContactViewModel)obj;
                        sw.WriteLine($"{vm.FirstName},{vm.LastName},{vm.Email},{vm.Message}");
                    }
                    isReturn = true;
                }
            }

            return isReturn;
        }
    }
}
